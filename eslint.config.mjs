import { iqrfEslint } from '@iqrf/eslint-config';

export default iqrfEslint({}, [
	{
		rules: {
			'no-irregular-whitespace': 'off',
		},
	},
	{
		files: [
			'docs/components/JsonApiExampleViewer.vue',
			'docs/components/JsonApiModal.vue',
			'docs/components/WebappApi.vue',
		],
		rules: {
			'vue-scoped-css/enforce-style-type': 'off',
		},
	},
]);
