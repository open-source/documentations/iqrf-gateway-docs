/// <reference types="vitepress" />
/// <reference types="vue3-toastify/global" />

declare module '*.vue' {
	import { type DefineComponent } from 'vue';
	const component: DefineComponent<Record<string, unknown>, Record<string, unknown>, unknown>;
	export default component;
}
