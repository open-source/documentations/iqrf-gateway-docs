/**
 * Component state
 */
export enum ComponentState {
	/// Component is initializing
	Init = 'init',
	/// Data loading finished
	Loaded = 'loaded',
	/// Component is loading data
	Loading = 'loading',
	/// Data loading failed
	LoadingFailed = 'loadingFailed',
}
