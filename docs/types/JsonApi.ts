import { type CompareOperator } from 'compare-versions';

/**
 * IQRF Gateway Daemon JSON API Item types
 */
export enum JsonApiItemTypes {
	/// Request & response
	RequestResponse = 'request+response',
	/// Async response
	AsyncResponse = 'asyncResponse',
	/// Message
	Message = 'message',
}

/**
 * IQRF Gateway Daemon JSON API Link types
 */
export enum JsonApiLinkTypes {
	/// Message example
	MessageExample = 'messageExample',
	/// Message schema
	MessageSchema = 'messageSchema',
	/// Request example
	RequestExample = 'requestExample',
	/// Request schema
	RequestSchema = 'requestSchema',
	/// Response example
	ResponseExample = 'responseExample',
	/// Response schema
	ResponseSchema = 'responseSchema',
}

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace JsonApiLinkTypes {

	/**
	 * Returns link type from string
	 * @param {JsonApiLinkTypes} linkType JSON API link type {@link JsonApiLinkTypes}
	 * @param {string | null} version Message version
	 * @param {string | null} mType Message type
	 * @return {string} Link type
	 */
	export function toString(linkType: JsonApiLinkTypes, version: string | null = null, mType: string | null = null): string {
		const sections: string[] = [];
		switch (linkType) {
			case JsonApiLinkTypes.MessageExample:
				sections.push('Message example');
				break;
			case JsonApiLinkTypes.MessageSchema:
				sections.push('Message schema');
				break;
			case JsonApiLinkTypes.RequestExample:
				sections.push('Request example');
				break;
			case JsonApiLinkTypes.RequestSchema:
				sections.push('Request schema');
				break;
			case JsonApiLinkTypes.ResponseExample:
				sections.push('Response example');
				break;
			case JsonApiLinkTypes.ResponseSchema:
				sections.push('Response schema');
				break;
		}
		if (mType !== null) {
			sections.push(`<code>${mType}</code>`);
		}
		if (version !== null) {
			sections.push(`v${version}`);
		}
		return sections.join(' ');
	}
}

/**
 * IQRF Gateway Daemon JSON API documentations
 */
export interface IJsonApiDocs {
	/// DPA documentation
	dpa?: string;
	/// IQRF standard documentation
	iqrfStandard?: string;
}

/**
 * IQRF Gateway Daemon JSON API badges
 */
export interface IJsonApiRawBadges {
	/// Is refactored?
	refactored?: boolean;
	/// Is tested?
	tested?: boolean;
}

/**
 * IQRF Gateway Daemon JSON API badges
 */
export interface IJsonApiBadges extends IJsonApiRawBadges {
	/// Asynchronous
	async?: boolean;
	/// Documentation badges
	docs?: IJsonApiDocs;
	/// Daemon version badges
	daemon?: {
		/// Since daemon version
		sinceVersion?: string;
	};
	/// DPA version badges
	dpa?: {
		/// Since DPA version
		sinceVersion?: string;
		/// Until DPA version
		untilVersion?: string;
	};
}

/**
 * IQRF Gateway Daemon JSON API change
 */
export type IJsonApiChange = string

/**
 * IQRF Gateway Daemon JSON API changelog entry
 */
export interface IJsonApiChangelogEntry {
	/// Version
	version: string;
	/// List of changes (descriptions)
	changes: IJsonApiChange[];
}

/**
 * IQRF Gateway Daemon JSON API changelog
 */
export type IJsonApiChangelog = IJsonApiChangelogEntry[];

/**
 * IQRF Gateway Daemon JSON API version
 */
export interface IJsonApiVersion {
	/// Compare operator
	operator: CompareOperator;
	/// Version
	version: string;
}

/**
 * IQRF Gateway Daemon JSON API version
 */
export interface IJsonApiVersions {
	/// IQRF Gateway Daemon version
	daemon: IJsonApiVersion[];
	/// DPA version
	dpa?: IJsonApiVersion[];
}

/**
 * IQRF Gateway Daemon JSON API item
 */
export interface IJsonApiItem {
	/// Item type
	type?: JsonApiItemTypes;
	/// Description
	description?: string;
	/// Documentation
	docs?: IJsonApiDocs;
	/// Message type
	mType: string;
	/// Name
	name: string;
	/// Version
	version?: string;
	/// Version definition
	versions?: IJsonApiVersions;
	/// Badges
	badges?: IJsonApiRawBadges;
	/// Changelog
	changelog?: IJsonApiChangelog;
}

/**
 * IQRF Gateway Daemon JSON API group
 */
export interface IJsonApiGroup {
	/// Description
	description?: string;
	/// Subgroups
	groups?: IJsonApiGroup[];
	/// Items
	items?: IJsonApiItem[];
	/// Name
	name: string;
	/// Version definition
	versions?: IJsonApiVersions;
	/// Badges
	badges?: IJsonApiRawBadges;
	/// Changelog
	changelog?: IJsonApiChangelog;
}

/**
 * Version parts
 */
export interface IVersionParts {
	/// Major version
	major?: number;
	/// Minor version
	minor?: number;
	/// Patch version
	patch?: number;
}
