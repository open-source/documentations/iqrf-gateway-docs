import vuetify from 'vite-plugin-vuetify';
import { defineConfig } from 'vitepress';

/// Base URL of the website
const baseUrl = '/iqrf-gateway/';

/**
 * Returns URL with base URL
 * @param {string} path Path
 * @return {string} URL with base URL
 */
function withBase(path: string): string {
	return `${baseUrl.replace(/\/$/, '')}/${path.replace(/^\//, '')}`;
}

/**
 * Defined VitePress configuration
 */
export default defineConfig({
	lang: 'en-GB',
	title: 'IQRF Gateway documentation',
	description: 'Documentation for IQRF Gateways',
	base: baseUrl,
	head: [
		['meta', { name: 'theme-color', content: '#367fa9' }],
		['link', { rel: 'apple-touch-icon', href: withBase('/apple-touch-icon.png'), sizes: '180x180' }],
		['link', { rel: 'icon', href: withBase('/favicon.svg'), type: 'image/svg+xml' }],
		['link', { rel: 'icon', type: 'image/png', href: withBase('favicon-32x32.png'), sizes: '32x32' }],
		['link', { rel: 'icon', type: 'image/png', href: withBase('favicon-16x16.png'), sizes: '16x16' }],
		['link', { rel: 'alternate icon', href: withBase('favicon.ico') }],
		['link', { rel: 'mask-icon', href: withBase('favicon.svg'), color: '#367fa9' }],
		['link', { rel: 'mask-icon', href: withBase('/safari-pinned-tab.svg'), color: '#367fa9' }],
	],
	outDir: '../dist/',
	cleanUrls: true,
	lastUpdated: true,
	markdown: {
		theme: {
			dark: 'material-theme-darker',
			light: 'material-theme-lighter',
		},
	},
	themeConfig: {
		search: {
			provider: 'algolia',
			options: {
				appId: 'QESX9SFRZ9',
				apiKey: 'bcb827b21d916d16e6925b34ff6fe455',
				indexName: 'iqrf-gateway',
			},
		},
		editLink: {
			pattern: 'https://gitlab.iqrf.org/open-source/iqrf-gateway-docs/-/blob/master/docs/:path',
		},
		logo: '/logo.svg',
		nav: [
			{
				text: 'User',
				link: '/user/',
				activeMatch: '/user/',
			},
			{
				text: 'Developer',
				link: '/developer/',
				activeMatch: '/developer/',
			},
			{
				text: 'Other docs',
				items: [
					{ text: 'IQUBE gateway', link: 'https://docs.iqrf.org/iqube/' },
					{ text: 'Industrial gateway', link: 'https://docs.iqrf.org/industrial/' },
				],
			},
		],
		outline: [2, 3],
		siteTitle: false,
		sidebar: {
			'/developer/': [
				{
					items: [
						{ text: 'Introduction', link: '/developer/introduction' },
					],
				},
			],
			'/user/': [
				{
					items: [
						{ text: 'News', link: '/user/news' },
						{ text: 'Roadmap', link: '/user/roadmap' },
						{ text: 'Introduction', link: '/user/introduction' },
						{ text: 'Getting started', link: '/user/getting-started' },
						{ text: 'Deployment guide', link: '/user/deployment-guide' },
					],
				},
				{
					text: 'IQRF Gateway Daemon',
					items: [
						{ text: 'Installation', link: '/user/daemon/installation' },
						{ text: 'Configuration', link: '/user/daemon/configuration' },
						{ text: 'Service mode', link: '/user/daemon/service-mode' },
						{
							text: 'JSON API',
							link: '/user/daemon/api',
							items: [
								{ text: 'v2.6.x', link: '/user/daemon/api/v260' },
								{ text: 'v2.5.x', link: '/user/daemon/api/v250' },
								{ text: 'v2.4.x', link: '/user/daemon/api/v240' },
								{ text: 'v2.3.x', link: '/user/daemon/api/v230' },
							],
						},
						{ text: 'Scheduler', link: '/user/daemon/scheduler' },
					],
				},
				{
					text: 'IQRF Gateway Webapp',
					items: [
						{ text: 'Installation', link: '/user/webapp/installation' },
						{ text: 'REST API', link: '/user/webapp/api' },
						{ text: 'CLI', link: '/user/webapp/cli' },
					],
				},
				{
					items: [
						{ text: 'Mender OTA', link: '/user/mender' },
						{ text: 'Docker', link: '/user/docker' },
						{ text: 'GitLab', link: '/user/gitlab' },
						{ text: 'Links', link: '/user/links' },
					],
				},
			],
		},
	},
	vite: {
		build: {
			commonjsOptions: {
				transformMixedEsModules: true,
			},
		},
		css: {
			preprocessorOptions: {
				scss: {
					api: 'modern-compiler',
				},
			},
		},
		define: {
			'process': { env: {} },
		},
		plugins: [
			vuetify({
				autoImport: true,
			}),
		],
		ssr: {
			noExternal: ['vuetify', '@stoplight/json-schema-viewer', '@stoplight/mosaic'],
		},
	},
});
