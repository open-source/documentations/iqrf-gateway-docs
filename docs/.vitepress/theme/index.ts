import mediumZoom from 'medium-zoom';
import nprogress from 'nprogress';
import { type EnhanceAppContext, type Route, type Router, useRoute } from 'vitepress';
import { default as DefaultTheme } from 'vitepress/theme';
import { nextTick, onMounted, watch } from 'vue';
import toastify, { type ToastContainerOptions } from 'vue3-toastify';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg';
import * as labsComponents from 'vuetify/labs/components';

import './custom.scss';

const toastifyOptions: ToastContainerOptions = {
	position: 'top-right',
	autoClose: 5_000,
	closeButton: true,
	pauseOnHover: true,
	pauseOnFocusLoss: true,
	closeOnClick: true,
	theme: 'colored',
	multiple: true,
	limit: 5,
	newestOnTop: true,
};

const vuetify = createVuetify({
	components: {
		...components,
		...labsComponents,
	},
	icons: {
		defaultSet: 'mdi',
		aliases,
		sets: {
			mdi,
		},
	},
	theme: {
		themes: {
			dark: {
				colors: {
					primary: '#367fa9',
					background: '#161618',
				},
			},
			light: {
				colors: {
					primary: '#367fa9',
					background: '#f6f6f7',
				},
			},
		},
	},
});

/**
 * Initializes nprogress
 * @param {Router} router Router
 */
function initNprogress(router: Router): void {
	if (typeof window === 'undefined') {
		return;
	}
	nprogress.configure({ showSpinner: false });
	const originalOnAfterRouteChange = router.onAfterRouteChange;
	router.onAfterRouteChange = async (to: string): Promise<void> => {
		nprogress.done();
		await originalOnAfterRouteChange?.(to);
	};
	const originalOnBeforeRouteChange = router.onBeforeRouteChange;
	router.onBeforeRouteChange = async (to: string): Promise<void> => {
		nprogress.start();
		await originalOnBeforeRouteChange?.(to);
	};
}

export default {
	...DefaultTheme,
	enhanceApp({ app, router }: EnhanceAppContext): void {
		app.use(toastify, toastifyOptions);
		app.use(vuetify);
		initNprogress(router);
	},
	setup(): void {
		const route: Route = useRoute();
		const initZoom = (): void => {
			mediumZoom('.vp-doc img', { background: 'var(--vp-c-bg)' });
		};
		onMounted((): void => {
			initZoom();
		});
		watch((): string => route.path, (): Promise<void> => nextTick((): void => {
			initZoom();
		}));
	},
};
