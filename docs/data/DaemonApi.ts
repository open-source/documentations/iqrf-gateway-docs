import { ref } from 'vue';

import { type IJsonApiGroup, JsonApiItemTypes } from '../types/JsonApi';

const groups: IJsonApiGroup[] = [
	{
		name: 'IQRF Generic',
		description: 'Generic messages are able to handle any DPA packet.',
		items: [
			{
				name: 'Raw',
				mType: 'iqrfRaw',
			},
			{
				name: 'RawHdp',
				mType: 'iqrfRawHdp',
			},
		],
	},
	{
		name: 'IQRF Standard',
		description: 'Standard messages have been designed according to <a href="https://www.iqrfalliance.org/iqrf-interoperability/" target="_blank" rel="noreferrer">IQRF Standard</a> and <a href="https://doc.iqrf.org/DpaTechGuide/" target="_blank" rel="noreferrer">DPA protocol</a>.',
		groups: [
			{
				name: 'Binary output',
				items: [
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardBinaryOutput/pages/enumerate-outputs.html',
						},
						name: 'Enumerate',
						mType: 'iqrfBinaryoutput_Enumerate',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In response, the `$.data.rsp.result.binOuts` key was renamed to `count`.',
								],
							},
						],
					},
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardBinaryOutput/pages/set-output.html',
						},
						name: 'Set output',
						mType: 'iqrfBinaryoutput_SetOutput',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.binOuts` key was renamed to `outputs`.',
									'In response, the `$.data.rsp.result.prevVals` key was renamed to `previousStates`.',
								],
							},
						],
					},
				],
			},
			{
				name: 'DALI',
				items: [
					{
						name: 'Send commands',
						mType: 'iqrfDali_SendCommands',
					},
					{
						name: 'Send commands asynchronously',
						mType: 'iqrfDali_SendCommandsAsync',
					},
					{
						name: 'FRC',
						mType: 'iqrfDali_Frc',
					},
				],
				versions: {
					daemon: [
						{ version: '2.6.0', operator: '<' },
					],
				},
			},
			{
				name: 'Light',
				items: [
					{
						name: 'Enumerate',
						mType: 'iqrfLight_Enumerate',
					},
					{
						name: 'Set power',
						mType: 'iqrfLight_SetPower',
					},
					{
						name: 'Increment power',
						mType: 'iqrfLight_IncrementPower',
					},
					{
						name: 'Decrement power',
						mType: 'iqrfLight_DecrementPower',
					},
				],
				versions: {
					daemon: [
						{ version: '2.6.0', operator: '<' },
					],
				},
			},
			{
				name: 'Light',
				items: [
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardLight/pages/set-lai--0x02.html',
						},
						name: 'Set Voltage of lighting analog interface (LAI)',
						mType: 'iqrfLight_SetLai',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.voltage` key was renamed to `ctrlSignal`.',
									'In response, the `$.data.rsp.result.prevVoltage` key was renamed to `ctrlSignal`.',
								],
							},
						],
					},
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardLight/pages/send-ldi-commands---0x00.html',
						},
						name: 'Send LDI commands and returns answers synchronously',
						mType: 'iqrfLight_SendLdiCommands',
					},
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardLight/pages/send-ldi-commands-asynchronously---0x01.html',
						},
						name: 'Send LDI commands and returns answers asynchronously',
						mType: 'iqrfLight_SendLdiCommandsAsync',
					},
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardLight/pages/read-lai---0xe1.html',
						},
						name: 'Read Voltage of lighting analog interface (LAI) using FRC',
						mType: 'iqrfLight_FrcLaiRead',
					},
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardLight/pages/send-ldi---0xe0.html',
						},
						name: 'Execute LDI command using FRC',
						mType: 'iqrfLight_FrcLdiSend',
					},
				],
				versions: {
					daemon: [
						{ version: '2.6.0', operator: '>=' },
					],
				},
			},
			{
				name: 'Sensor',
				items: [
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/enumerate-sensors.html',
						},
						name: 'Enumerate',
						mType: 'iqrfSensor_Enumerate',
					},
					{
						docs:{
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/read-sensors-with-types.html',
						},
						name: 'Read sensors with types',
						mType: 'iqrfSensor_ReadSensorsWithTypes',
					},
					{
						docs: {
							iqrfStandard: 'https://doc.iqrf.org/IQRF-Standards/StandardSensor/pages/frc-commands.html',
						},
						name: 'FRC',
						mType: 'iqrfSensor_Frc',
					},
				],
			},
			{
				name: 'Embed Explore',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/peripheral-enumeration.html',
						},
						name: 'Enumerate',
						mType: 'iqrfEmbedExplore_Enumerate',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/PeripheralInfo.html',
						},
						name: 'Peripheral information',
						mType: 'iqrfEmbedExplore_PeripheralInformation',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/get-information-for-more-peripherals.html',
						},
						name: 'More peripheral information',
						mType: 'iqrfEmbedExplore_MorePeripheralsInformation',
					},
				],
			},
			{
				name: 'Embed Coordinator',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/get-addressing-information.html',
						},
						name: 'Address information',
						mType: 'iqrfEmbedCoordinator_AddrInfo',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/get-discovered-nodes.html',
						},
						name: 'Get discovered devices',
						mType: 'iqrfEmbedCoordinator_DiscoveredDevices',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/get-bonded-nodes.html',
						},
						name: 'Get bonded devices',
						mType: 'iqrfEmbedCoordinator_BondedDevices',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/clear-all-bonds.html',
						},
						name: 'Clear all bonds',
						mType: 'iqrfEmbedCoordinator_ClearAllBonds',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/bond-node.html',
						},
						name: 'Bond node',
						mType: 'iqrfEmbedCoordinator_BondNode',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/remove-bonded-node.html',
						},
						name: 'Remove bond',
						mType: 'iqrfEmbedCoordinator_RemoveBond',
					},
					{
						name: 'Re-bond node',
						mType: 'iqrfEmbedCoordinator_ReBondNode',
						versions: {
							daemon: [{ version: '2.0.0', operator: '<' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/discovery.html',
						},
						name: 'Discovery',
						mType: 'iqrfEmbedCoordinator_Discovery',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/set-dpa-param.html',
						},
						name: 'Set DPA parameters',
						mType: 'iqrfEmbedCoordinator_SetDpaParams',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In response, the `$.data.rsp.result.prevDpaParam` key was renamed to `dpaParam`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/set-hops.html',
						},
						name: 'Set hops',
						mType: 'iqrfEmbedCoordinator_SetHops',
					},
					{
						name: 'Discovery data',
						mType: 'iqrfEmbedCoordinator_DiscoveryData',
						versions: {
							daemon: [{ version: '2.5.0', operator: '<=' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/CoordinatorBackup.html',
						},
						name: 'Backup',
						mType: 'iqrfEmbedCoordinator_Backup',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/CoordinatorRestore.html',
						},
						name: 'Restore',
						mType: 'iqrfEmbedCoordinator_Restore',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.netData` key was renamed to `networkData`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/authorize-bond.html',
						},
						name: 'Authorize bond',
						mType: 'iqrfEmbedCoordinator_AuthorizeBond',
					},
					{
						name: 'Read remotely bonded MID',
						mType: 'iqrfEmbedCoordinator_ReadRemotelyBondedMid',
						versions: {
							daemon: [{ version: '2.5.0', operator: '<=' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						name: 'Clear remotely bonded MID',
						mType: 'iqrfEmbedCoordinator_ClearRemotelyBondedMid',
						versions: {
							daemon: [{ version: '2.5.0', operator: '<=' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						name: 'Enable remote bonding',
						mType: 'iqrfEmbedCoordinator_EnableRemoteBonding',
						versions: {
							daemon: [{ version: '2.5.0', operator: '<=' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/smart-connect.html',
						},
						name: 'SmartConnect',
						mType: 'iqrfEmbedCoordinator_SmartConnect',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/set-mid.html',
						},
						name: 'Set MID',
						mType: 'iqrfEmbedCoordinator_SetMID',
					},
				],
			},
			{
				name: 'Embed Node',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/416/pages/NodeRead.html',
						},
						name: 'Read',
						mType: 'iqrfEmbedNode_Read',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/416/pages/remove-bond.html',
						},
						name: 'Remove bond',
						mType: 'iqrfEmbedNode_RemoveBond',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/416/pages/NodeBackup.html',
						},
						name: 'Backup',
						mType: 'iqrfEmbedNode_Backup',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In response, the `$.data.rsp.result.backupData` key was renamed to `networkData`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/416/pages/NodeRestore.html',
						},
						name: 'Restore',
						mType: 'iqrfEmbedNode_Restore',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.backupData` key was renamed to `networkData`.',
								],
							},
						],
					},
					{
						name: 'Read remotely bonded MID',
						mType: 'iqrfEmbedNode_ReadRemotelyBondedMid',
						versions: {
							daemon: [{ version: '2.5.0', operator: '<=' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						name: 'Clear remotely bonded MID',
						mType: 'iqrfEmbedNode_ClearRemotelyBondedMid',
						versions: {
							daemon: [{ version: '2.5.0', operator: '<=' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						name: 'Enable remote bonding',
						mType: 'iqrfEmbedNode_EnableRemoteBonding',
						versions: {
							daemon: [{ version: '2.5.0', operator: '<=' }],
							dpa: [{ version: '4.0.0', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/416/pages/validate-bonds.html',
						},
						name: 'Validate bonds',
						mType: 'iqrfEmbedNode_ValidateBonds',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.nodes.bondAddr` key was renamed to `address`.',
								],
							},
						],
					},
				],
			},
			{
				name: 'Embed OS',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/OsRead.html',
						},
						name: 'Read',
						mType: 'iqrfEmbedOs_Read',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/batch.html',
						},
						name: 'Batch',
						mType: 'iqrfEmbedOs_Batch',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.requests.pnum` key was renamed to `pNum` and switched from string to integer.',
									'In request, the `$.data.req.param.requests.pcmd` key was renamed to `pCmd` and switched from string to integer.',
									'In request, the `$.data.req.param.requests.hwpid` key was renamed to `hwpId` and switched from string to integer.',
									'In request, the `$.data.req.param.requests.rdata` key was renamed to `rData`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/selective-batch.html',
						},
						name: 'Selective batch',
						mType: 'iqrfEmbedOs_SelectiveBatch',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.requests.pnum` key was renamed to `pNum` and switched from string to integer.',
									'In request, the `$.data.req.param.requests.pcmd` key was renamed to `pCmd` and switched from string to integer.',
									'In request, the `$.data.req.param.requests.hwpid` key was renamed to `hwpId` and switched from string to integer.',
									'In request, the `$.data.req.param.requests.rdata` key was renamed to `rData`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/OsReset.html',
						},
						name: 'Reset',
						mType: 'iqrfEmbedOs_Reset',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/restart.html',
						},
						name: 'Restart',
						mType: 'iqrfEmbedOs_Restart',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/run-rfpgm.html',
						},
						name: 'Run RFPGM',
						mType: 'iqrfEmbedOs_Rfpgm',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/set-security.html',
						},
						name: 'Set security',
						mType: 'iqrfEmbedOs_SetSecurity',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/sleep.html',
						},
						name: 'Sleep',
						mType: 'iqrfEmbedOs_Sleep',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/loadcode.html',
						},
						name: 'Load code',
						mType: 'iqrfEmbedOs_LoadCode',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/read-tr-configuration.html',
						},
						name: 'Read configuration',
						mType: 'iqrfEmbedOs_ReadCfg',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In response, the `$.data.rsp.result.undocumented` key was renamed to `initphy`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/write-tr-configuration.html',
						},
						name: 'Write configuration',
						mType: 'iqrfEmbedOs_WriteCfg',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.checksum` key was removed.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/write-tr-configuration-byte.html',
						},
						name: 'Write configuration byte',
						mType: 'iqrfEmbedOs_WriteCfgByte',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/OsTestRfSignal.html',
						},
						name: 'Test RF signal',
						mType: 'iqrfEmbedOs_TestRfSignal',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/factory-settings.html',
						},
						name: 'Factory settings',
						mType: 'iqrfEmbedOs_FactorySettings',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/IndicateCommand.html',
						},
						name: 'Indicate',
						mType: 'iqrfEmbedOs_Indicate',
					},
				],
			},
			{
				name: 'Embed EEPROM',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/EEPROMRead.html',
						},
						name: 'Read',
						mType: 'iqrfEmbedEeprom_Read',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.len` key was renamed to `length`',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/EEPROMWrite.html',
						},
						name: 'Write',
						mType: 'iqrfEmbedEeprom_Write',
					},
				],
			},
			{
				name: 'Embed EEEPROM',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/extended-read.html',
						},
						name: 'Read',
						mType: 'iqrfEmbedEeeprom_Read',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.len` key was renamed to `length`',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'hhttps://doc.iqrf.org/DpaTechGuide/pages/extended-write.html',
						},
						name: 'Write',
						mType: 'iqrfEmbedEeeprom_Write',
					},
				],
			},
			{
				name: 'Embed RAM',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/ram.html',
						},
						name: 'Read',
						mType: 'iqrfEmbedRam_Read',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/ram.html',
						},
						name: 'Write',
						mType: 'iqrfEmbedRam_Write',
					},
				],
			},
			{
				name: 'Embed SPI',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/414/index.html?page=SpiPeripheral.html',
						},
						name: 'Write and read',
						mType: 'iqrfEmbedSpi_WriteRead',
						versions: {
							daemon: [{ version: '3.0.0', operator: '<' }],
							dpa: [{ version: '4.15.0', operator: '<' }],
						},
					},
				],
				versions: {
					daemon: [{ version: '3.0.0', operator: '<' }],
					dpa: [{ version: '4.15.0', operator: '<' }],
				},
			},
			{
				name: 'Embed LEDR',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/LedSet.html',
						},
						name: 'Set',
						mType: 'iqrfEmbedLedr_Set',
						versions: {
							daemon: [{ version: '3.0.0', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/LedSet.html',
						},
						name: 'SetOff',
						mType: 'iqrfEmbedLedr_SetOff',
						versions: {
							daemon: [{ version: '2.5.0', operator: '>=' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/LedSet.html',
						},
						name: 'SetOn',
						mType: 'iqrfEmbedLedr_SetOn',
						versions: {
							daemon: [{ version: '2.5.0', operator: '>=' }],
						},
					},
					{
						name: 'Get',
						mType: 'iqrfEmbedLedr_Get',
						versions: {
							daemon: [{ version: '3.0.0', operator: '<' }],
							dpa: [{ version: '3.0.3', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/pulse.html',
						},
						name: 'Pulse',
						mType: 'iqrfEmbedLedr_Pulse',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/flashing.html',
						},
						name: 'Flashing',
						mType: 'iqrfEmbedLedr_Flashing',
					},
				],
			},
			{
				name: 'Embed LEDG',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/LedSet.html',
						},
						name: 'Set',
						mType: 'iqrfEmbedLedg_Set',
						versions: {
							daemon: [{ version: '3.0.0', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/LedSet.html',
						},
						name: 'SetOff',
						mType: 'iqrfEmbedLedg_SetOff',
						versions: {
							daemon: [{ version: '2.5.0', operator: '>=' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/LedSet.html',
						},
						name: 'SetOn',
						mType: 'iqrfEmbedLedg_SetOn',
						versions: {
							daemon: [{ version: '2.5.0', operator: '>=' }],
						},
					},
					{
						name: 'Get',
						mType: 'iqrfEmbedLedg_Get',
						versions: {
							daemon: [{ version: '3.0.0', operator: '<' }],
							dpa: [{ version: '3.0.3', operator: '<' }],
						},
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/pulse.html',
						},
						name: 'Pulse',
						mType: 'iqrfEmbedLedg_Pulse',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/flashing.html',
						},
						name: 'Flashing',
						mType: 'iqrfEmbedLedg_Flashing',
					},
				],
			},
			{
				name: 'Embed IO',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/direction.html',
						},
						name: 'Direction',
						mType: 'iqrfEmbedIo_Direction',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.ports` key was renamed to `subcommands`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/IoSet.html',
						},
						name: 'Set',
						mType: 'iqrfEmbedIo_Set',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.param.ports` key was renamed to `subcommands`.',
								],
							},
						],
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/IoGet.html',
						},
						name: 'Get',
						mType: 'iqrfEmbedIo_Get',
					},
				],
			},
			{
				name: 'Embed Thermometer',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/ThermometerRead.html',
						},
						name: 'Read',
						mType: 'iqrfEmbedThermometer_Read',
					},
				],
			},
			{
				name: 'Embed UART',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/OpenUart.html',
						},
						name: 'Open',
						mType: 'iqrfEmbedUart_Open',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/CloseUart.html',
						},
						name: 'Close',
						mType: 'iqrfEmbedUart_Close',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/UartWriteRead.html',
						},
						name: 'Write and read',
						mType: 'iqrfEmbedUart_WriteRead',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/clear--write--read.html',
						},
						name: 'Clear, write and read',
						mType: 'iqrfEmbedUart_ClearWriteRead',
					},
				],
			},
			{
				name: 'Embed FRC',
				items: [
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/SendFrc.html',
						},
						name: 'Send',
						mType: 'iqrfEmbedFrc_Send',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/extra-result.html',
						},
						name: 'Extra result',
						mType: 'iqrfEmbedFrc_ExtraResult',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/send-selective.html',
						},
						name: 'Send selective',
						mType: 'iqrfEmbedFrc_SendSelective',
					},
					{
						docs: {
							dpa: 'https://doc.iqrf.org/DpaTechGuide/pages/set-frc-params.html',
						},
						name: 'Set parameters',
						mType: 'iqrfEmbedFrc_SetParams',
					},
				],
			},
		],
	},
	{
		name: 'IQRF Sensor Data',
		description: 'The sensor data service allows for periodic collecting of sensor measurement data and storing latest values in the database.',
		items: [
			{
				name: 'Get configuration',
				mType: 'iqrfSensorData_GetConfig',
			},
			{
				name: 'Set configuration',
				mType: 'iqrfSensorData_SetConfig',
			},
			{
				name: 'Start worker',
				mType: 'iqrfSensorData_Start',
			},
			{
				name: 'Stop worker',
				mType: 'iqrfSensorData_Stop',
			},
			{
				name: 'Invoke worker',
				mType: 'iqrfSensorData_Invoke',
			},
			{
				name: 'Worker status',
				mType: 'iqrfSensorData_Status',
			},
			{
				name: 'Asynchronous report',
				mType: 'iqrfSensorData_ReportAsync',
				type: JsonApiItemTypes.AsyncResponse,
			},
		],
		versions: {
			daemon: [
				{ version: '3.0.0', operator: '>=' },
			],
		},
	},
	{
		name: 'IQMESH Network',
		description: 'Services that ease the task of working with IQMESH network.<br>They are composed of more than single DPA transaction (request-confirmation-response) in most of the cases.<br>They are also integrating information from <a href="https://repository.iqrfalliance.org/doc/" target="_blank" rel="noreferrer">IQRF Repository</a>.<br>They are inspired by the services available in <a href="https://iqrf.org/technology/iqrf-ide" target="_blank" rel="noreferrer">IQRF IDE</a> - IQMESH Network Manager.',
		groups: [
			{
				name: 'IQRF Network',
				items: [
					{
						name: 'Ping',
						mType: 'iqmeshNetwork_Ping',
					},
					{
						name: 'Restart',
						mType: 'iqmeshNetwork_Restart',
					},
				],
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
			{
				name: 'IQRF Bonding',
				items: [
					{
						name: 'Bond node locally',
						mType: 'iqmeshNetwork_BondNodeLocal',
						changelog: [
							{
								version: '3.0.0',
								changes: [
									'In request, the `$.data.req.bondingMask` was removed.',
								],
							},
						],
					},
					{
						name: 'SmartConnect',
						mType: 'iqmeshNetwork_SmartConnect',
					},
					{
						name: 'Remove bond',
						mType: 'iqmeshNetwork_RemoveBond',
					},
					{
						name: 'Remove bond only from coordinator',
						mType: 'iqmeshNetwork_RemoveBondOnlyInC',
						versions: {
							daemon: [{ version: '3.0.0', operator: '<' }],
						},
					},
					{
						name: 'AutoNetwork',
						mType: 'iqmeshNetwork_AutoNetwork',
					},
				],
			},
			{
				name: 'IQRF Enumeration',
				items: [
					{
						name: 'Enumerate device',
						mType: 'iqmeshNetwork_EnumerateDevice',
					},
				],
			},
			{
				name: 'IQRF Configuration',
				items: [
					{
						name: 'Read TR configuration',
						mType: 'iqmeshNetwork_ReadTrConf',
					},
					{
						name: 'Write TR configuration',
						mType: 'iqmeshNetwork_WriteTrConf',
					},
				],
			},
			{
				name: 'IQRF DPA Parameters',
				items: [
					{
						name: 'DPA parameters',
						mType: 'iqmeshNetwork_DpaValue',
					},
					{
						name: 'DPA hops',
						mType: 'iqmeshNetwork_DpaHops',
					},
					{
						name: 'FRC parameters',
						mType: 'iqmeshNetwork_FrcParams',
					},
				],
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
			{
				name: 'IQRF Backup and Restore',
				items: [
					{
						name: 'Backup',
						mType: 'iqmeshNetwork_Backup',
					},
					{
						name: 'Restore',
						mType: 'iqmeshNetwork_Restore',
					},
				],
			},
			{
				name: 'IQRF OTA',
				items: [
					{
						name: 'Upload',
						mType: 'iqmeshNetwork_OtaUpload',
					},
				],
			},
			{
				name: 'IQRF Maintenance',
				items: [
					{
						name: 'FRC response time',
						mType: 'iqmeshNetwork_MaintenanceFrcResponseTime',
					},
					{
						name: 'Test RF signal',
						mType: 'iqmeshNetwork_MaintenanceTestRF',
					},
					{
						name: 'Resolve duplicated addresses',
						mType: 'iqmeshNetwork_MaintenanceDuplicatedAddresses',
					},
					{
						name: 'Resolve inconsistent MIDs in Coordinator',
						mType: 'iqmeshNetwork_MaintenanceInconsistentMIDsInCoord',
					},
					{
						name: 'Resolve unused prebonded nodes',
						mType: 'iqmeshNetwork_MaintenanceUselessPrebondedNodes',
					},
				],
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
		],
	},
	{
		name: 'Daemon Management',
		items: [
			{
				name: 'Exit',
				mType: 'mngDaemon_Exit',
			},
			{
				name: 'Mode',
				mType: 'mngDaemon_Mode',
				versions: {
					daemon: [{ version: '3.0.0', operator: '<' }],
				},
			},
			{
				name: 'Get operating mode',
				mType: 'mngDaemon_GetMode',
				versions: {
					daemon: [{ version: '3.0.0', operator: '>=' }],
				},
			},
			{
				name: 'Set operating mode',
				mType: 'mngDaemon_SetMode',
				versions: {
					daemon: [{ version: '3.0.0', operator: '>=' }],
				},
			},
			{
				name: 'Reload coordinator',
				mType: 'mngDaemon_ReloadCoordinator',
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
			{
				name: 'Update cache',
				mType: 'mngDaemon_UpdateCache',
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
			{
				name: 'Version',
				mType: 'mngDaemon_Version',
			},
		],
		changelog: [
			{
				version: '3.0.0',
				changes: [
					'The `mngDaemon_Mode` messages were removed and substituted with `mngDaemon_GetMode` and `mngDaemon_SetMode`.',
				],
			},
		],
	},
	{
		name: 'Daemon Database',
		items: [
			{
				name: 'Device enumeration',
				mType: 'iqrfDb_Enumerate',
			},
			{
				name: 'Get device information',
				mType: 'iqrfDb_GetDevice',
			},
			{
				name: 'Get information about multiple devices',
				mType: 'iqrfDb_GetDevices',
			},
			{
				name: 'Get user-defined device metadata',
				mType: 'iqrfDb_GetDeviceMetadata',
			},
			{
				name: 'Set user-defined device metadata',
				mType: 'iqrfDb_SetDeviceMetadata',
			},
			{
				name: 'Get binary outputs',
				mType: 'iqrfDb_GetBinaryOutputs',
			},
			{
				name: 'Get lights',
				mType: 'iqrfDb_GetLights',
			},
			{
				name: 'Get sensors',
				mType: 'iqrfDb_GetSensors',
			},
			{
				name: 'Get network topology',
				mType: 'iqrfDb_GetNetworkTopology',
			},
			{
				name: 'Reset database',
				mType: 'iqrfDb_Reset',
			},
		],
		versions: {
			daemon: [
				{ version: '3.0.0', operator: '>=' },
			],
		},
	},
	{
		name: 'Daemon Configuration',
		items: [
			{
				name: 'Component',
				mType: 'cfgDaemon_Component',
			},
		],
	},
	{
		name: 'Daemon Scheduler',
		items: [
			{
				name: 'Add task',
				mType: 'mngScheduler_AddTask',
			},
			{
				name: 'Edit task',
				mType: 'mngScheduler_EditTask',
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
			{
				name: 'Get task',
				mType: 'mngScheduler_GetTask',
			},
			{
				name: 'List tasks',
				mType: 'mngScheduler_List',
			},
			{
				name: 'Remove all tasks',
				mType: 'mngScheduler_RemoveAll',
			},
			{
				name: 'Remove task',
				mType: 'mngScheduler_RemoveTask',
			},
			{
				name: 'Start task',
				mType: 'mngScheduler_StartTask',
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
			{
				name: 'Stop task',
				mType: 'mngScheduler_StopTask',
				versions: {
					daemon: [{ version: '2.4.0', operator: '>=' }],
				},
			},
		],
	},
	{
		name: 'Daemon Metadata',
		items: [
			{
				name: 'Set metadata',
				mType: 'mngMetaData_SetMetaData',
			},
			{
				name: 'Get metadata',
				mType: 'mngMetaData_GetMetaData',
			},
			{
				name: 'Set MID to metadata',
				mType: 'mngMetaData_SetMidMetaId',
			},
			{
				name: 'Get metadata from MID',
				mType: 'mngMetaData_GetMidMetaData',
			},
			{
				name: 'Get metadata from NADR',
				mType: 'mngMetaData_GetNadrMetaData',
			},
			{
				name: 'Export metadata',
				mType: 'mngMetaData_ExportMetaDataAll',
			},
			{
				name: 'Verify metadata',
				mType: 'mngMetaData_VerifyMetaDataAll',
			},
			{
				name: 'Import metadata',
				mType: 'mngMetaData_ImportMetaDataAll',
			},
			{
				name: 'Import NADR-MID map',
				mType: 'mngMetaData_ImportNadrMidMap',
			},
			{
				name: 'Export NADR-MID map',
				mType: 'mngMetaData_ExportNadrMidMap',
			},
		],
		versions: {
			daemon: [{ version: '2.4.0', operator: '<' }],
		},
	},
	{
		name: 'Daemon Information',
		items: [
			{
				name: 'Enumeration',
				mType: 'infoDaemon_Enumeration',
			},
			{
				name: 'Get nodes',
				mType: 'infoDaemon_GetNodes',
			},
			{
				name: 'Get sensors',
				mType: 'infoDaemon_GetSensors',
			},
			{
				name: 'Get binary outputs',
				mType: 'infoDaemon_GetBinaryOutputs',
			},
			{
				name: 'Get lights',
				mType: 'infoDaemon_GetLights',
			},
			{
				name: 'Get DALI devices',
				mType: 'infoDaemon_GetDalis',
			},
			{
				name: 'Get metadata assigned to MID',
				mType: 'infoDaemon_GetMidMetaData',
			},
			{
				name: 'Get metadata assigned to bonded node',
				mType: 'infoDaemon_GetNodeMetaData',
			},
			{
				name: 'Annotate messages with metadata',
				mType: 'infoDaemon_MidMetaDataAnnotate',
			},
			{
				name: 'Handle orphaned MIDs',
				mType: 'infoDaemon_OrphanedMids',
			},
			{
				name: 'Reset',
				mType: 'infoDaemon_Reset',
			},
			{
				name: 'Set metadata to MID',
				mType: 'infoDaemon_SetMidMetaData',
			},
			{
				name: 'Set metadata to bonded node',
				mType: 'infoDaemon_SetNodeMetaData',
			},
		],
		versions: {
			daemon: [
				{ version: '2.4.0', operator: '>=' },
				{ version: '3.0.0', operator: '<' },
			],
		},
	},
	{
		name: 'Daemon Notification',
		description: 'Daemon state notifications related to interfaces, modes via WebSocket channel on port <code>1438</code>.',
		items: [
			{
				name: 'Monitor',
				mType: 'ntfDaemon_Monitor',
				type: JsonApiItemTypes.Message,
			},
			{
				name: 'Monitor invocation',
				mType: 'ntfDaemon_InvokeMonitor',
				versions: {
					daemon: [{ version: '2.5.3', operator: '>=' }],
				},
			},
		],
	},
];

export default ref(groups);
