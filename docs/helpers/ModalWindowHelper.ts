import { computed, type ComputedRef } from 'vue';
import { type DisplayInstance, useDisplay } from 'vuetify';

/**
 * Model window helpers
 */
export default class ModalWindowHelper {
	public static getWidth(): ComputedRef<string> {
		return computed((): string => {
			const display: DisplayInstance = useDisplay();
			if (display.lgAndUp.value) {
				return '50%';
			}
			if (display.md.value) {
				return '75%';
			}
			return '100%';
		});
	}
}
