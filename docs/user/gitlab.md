# Gitlab

IQRF Gateway team strongly believes in open source software and collaboration.

## Public repositories

* Source code

  * [IQRF Gateway Daemon](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon)
  * [IQRF Gateway Webapp](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp)
  * [IQRF Gateway Docs](https://gitlab.iqrf.org/open-source/iqrf-gateway-docs)

## Public development

* Tracking

  * [IQRF Gateway Daemon](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/milestones)
  * [IQRF Gateway Webapp](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/milestones)

* Improvements

  * [IQRF Gateway Daemon](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/merge_requests)
  * [IQRF Gateway Webapp](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/merge_requests)

* Documentation

  * [User Guide](https://docs.iqrf.org/iqrf-gateway/user)
  * [Developer Guide](https://docs.iqrf.org/iqrf-gateway/developer)

## Report bugs

* Create a new issue

  * [IQRF Gateway Daemon](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/issues)
  * [IQRF Gateway Webapp](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/issues)
  * [IQRF Gateway Docs](https://gitlab.iqrf.org/open-source/iqrf-gateway-docs/issues)

* Describe the issue and attach logs from the gateway
* Logs can be downloaded from the gateway
  via <http://IQRF-Gateway-Webapp-IP/gateway/info/> - Download diagnostics

We believe that with your active participation the IQRF Gateway can be greatly
improved.

Enjoy!

IQRF Gateway Team

<https://gitlab.iqrf.org/open-source>
