# Welcome to IQRF Gateway user's documentation!

Open-source components for building IQRF Gateway.

IQRF Gateway Daemon and IQRF Gateway Webapp projects provide open-source components
for building IQRF Gateways. Together they form ready-to-use solution. Components
can be extended or added based on the project requirements. All components are
licenced under Apache Licence 2.0 and can be used for commercial purposes.

## Acknowledgement

This project has been made possible with a government grant by means of
[the Ministry of Industry and Trade of the Czech Republic](https://www.mpo.cz/)
in [the TRIO program](https://starfos.tacr.cz/cs/project/FV40132).
