# Introduction

Open-source components for building IQRF Gateway.

[IQRF Gateway Daemon](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp)
project provides open-source components for building IQRF Gateways based on
Linux system. Together they form ready-to-use solution, including an user-friendly
[IQRF Gateway Webapp](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp).

Components can be extended or added based on the project's requirements. All
components are licenced under Apache Licence 2.0 and can be used for commercial
purposes.

## IQRF GW daemon

![IQRF Gateway Daemon overview](/user/iqrfgd-overview.png)

## IQRF GW webapp

![IQRF Gateway Webapp overview](/user/iqrfgw-overview.png)

## Why v2 and v3

- Modular design

  - Extendible based on project's requirements
  - Based on [Shape framework](https://github.com/logimic/shape)

- [IQRF Standard](/user/daemon/api#iqrf-standard) supported and exposed as JSON API

  - Working with IQRF Repository
  - Offline cached support

- [IQMESH Services](/user/daemon/api#iqmesh-network) introduced and exposed as JSON API

  - Convenient for system integrators
  - Leaving DPA protocol bits and bytes to us

- [IQRF Generic](/user/daemon/api#iqrf-generic) API is kept alive

  - Working with DPA bits and bytes as in GW Daemon v1

- Reworked IQMESH Manager in [Webapp](#iqrf-gw-webapp)

  - Making use of IQMESH Services as mentioned above

- [WebSocket](https://en.wikipedia.org/wiki/WebSocket) channel introduced

  - Enables Docker containers for End-apps more easily
  - TLS secured

- IQRF UART interface supported

  - Working with [IQUBE](https://docs.iqrf.org/iqube)
    , [Industrial](https://docs.iqrf.org/industrial)
    , [Iris](https://www.unipi.technology/cs/produkty/unipi-iris-458) and
    other boards

- Unicast, broadcast and FRC timing supported

  - GW Daemon knows correct time to wait for DPA response

- Growing base
  of [examples and reference apps](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/tree/master/examples)

  - Great inspiration on how to work with our JSON API from your favourite
    programming language

- Long term support

  - Have trouble,
    write [daemon issue](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/issues)
    or [webapp issue](https://gitlab.iqrf.org/open-source/iqrf-gateway-webapp/issues)
