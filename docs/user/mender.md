# Mender OTA

Update your **IQRF Gateway fleet** using [Mender](https://mender.io/) platform in just a few minutes.

## Mender server

| Our testing server is running here: <https://mender.iqrf.org>

| When building your image, edit Mender configuration to use your server or contact us at <support@iqrf.org> for a demo.

## Repository

Build your custom Linux Yocto image for Raspberry Pi with Mender support.

```bash
git clone https://gitlab.iqrf.org/open-source/iqrf-gateway-os
cd iqrf-gateway-os
./build_yocto_rpi.sh
```

## Download image

Download, extract and flash the OS image to your SD card or compute module using
[Etcher](https://www.balena.io/etcher/) SW.

The latest images (*.sdimg.bz2) can be found [here](https://dl.iqrf.org/iqrf-gateway-os/).

## Download artifact

Once your devices have been registered in Mender, server update can be performed using the artifacts from Web UI.

The latest artifacts (*.mender) can be found [here](https://dl.iqrf.org/iqrf-gateway-os/).
