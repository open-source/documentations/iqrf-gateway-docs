# Getting started

* Install the [IQRF Gateway Daemon](/user/daemon/installation)
* Install the [IQRF Gateway Webapp](/user/webapp/installation)
* [Configure](/user/daemon/configuration) your IQRF interface for coordinator TR
  module

  * Select **one of** from SPI, UART or CDC based on your HW

* Learn about the [Daemon API](/user/daemon/api)
  and [Webapp API](/user/webapp/api)
* [Scheduler](/user/daemon/scheduler) helps with regular tasks
* Choose [MQ](https://en.wikipedia.org/wiki/Message_queue)
  /[WebSocket](https://en.wikipedia.org/wiki/WebSocket)
  /[MQTT](https://en.wikipedia.org/wiki/MQTT) channel for communication with the
  daemon

  * Check/Set configuration for your channel using IQRF Gateway Webapp

## Next steps

* Use <http://webapp-ip/iqrfnet/send-raw/> to confirm communication with TR
  module in the gateway
* Use <http://webapp-ip/iqrfnet/network/> to **Bond via button**/**Smart Connect
  via QR code**/**AutoNetwork** new devices into the IQRF network
* Use <http://webapp-ip/cloud/{aws/azure/bluemix/inteli-glue/}> manager to
  connect the gateway to the **favourite cloud**
* Check gateway [Webapp API](/user/webapp/api)

* Configure any JSON API task in the [Scheduler](/user/daemon/scheduler) or
  send [JSON API](/user/daemon/api) requests from your application directly
* If you use IQRF standard devices such as Sensor, Binary output, Light or Dali
  in your network, check [JSON API for Standard](/user/daemon/api#iqrf-standard)
* Parse [JSON API](/user/daemon/api) responses coming from the network

## Reference applications

* [FRC&Sleep](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/tree/master/apps/frc&sleep)
* [Multiple GWs](https://docs.iqrf.org/iqube/tutorials.html)

## Examples

Give a go with the API examples in your favourite programming language

* [Bash](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/tree/master/examples/bash)
* [Python](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/tree/master/examples/python)
* [JavaScript](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/tree/master/examples/node.js)
* [NodeRED](https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/tree/master/examples/node-red)
