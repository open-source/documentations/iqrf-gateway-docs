# Links

Useful links for the quick orientation.

## IQRF technology start

* [How to start?](https://www.iqrf.org/how-to-start)
* [Video Tutorials](https://www.iqrf.org/support/videos)

## DPA protocol

* [DPA](https://www.iqrf.org/technology/dpa)
* [DPA Technical Guide](https://www.iqrf.org/DpaTechGuide/)

## IQRF Interoperability Standard

* [IQRF Interoperability](https://www.iqrfalliance.org/iqrf-interoperability/)

## IQRF Repository

* [IQRF Repository](https://www.iqrf.org/technology/iqrf-repository)
* [IQRF Repository documentation](https://repository.iqrfalliance.org/doc/)
* [GitLab repositories](https://gitlab.iqrf.org/open-source/iqrf-repository)

## IQRF SDK libs and examples

* [IQRFPY](https://apidocs.iqrf.org/iqrfpy/latest/iqrfpy.html)
* [SDK](https://www.iqrf.org/technology/iqrf-sdk)
* [GitHub](https://github.com/iqrfsdk)

## IoT Starter Kit

* [IQRF Alliance Marketplace](https://www.iqrfalliance.org/marketplace/iot-starter-kit)
* [GitHub repository](https://github.com/iqrfsdk/iot-starter-kit)

## IQRF Gateways

* [IQUBE](https://www.iqrf.org/product-detail/iqd-gw-02-iqube)
* [INDUSTRIAL](https://www.iqrf.org/product-detail/iqd-gw04)

## Mobile IQRF Network Manager

* [Google Play](https://play.google.com/store/apps/details?id=org.iqrfalliance.demo)
