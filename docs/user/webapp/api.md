---
aside: false
layout: page
title: Webapp API
---
<script setup lang='ts'>
import { useSidebar } from 'vitepress/dist/client/theme-default/composables/sidebar';
import VPDocFooter from 'vitepress/dist/client/theme-default/components/VPDocFooter.vue';

const { hasSidebar } = useSidebar();

import WebappApi from '/components/WebappApi.vue';
</script>
<div
	class="VPDoc"
	:class="{ 'has-sidebar': hasSidebar }"
>
	<div class="container">
		<div class="content">
			<div class="content-container">
				<slot name="doc-before" />
				<main class='main vp-doc'>
					<h1 id="webapp-api" tabindex="-1">
						Webapp API
						<a class="header-anchor" href="#webapp-api" aria-label="Permalink to &quot;Webapp API&quot;">​</a>
					</h1>
					<p>REST API to manage/configure different SW parts of the IQRF Gateway. </p>
					<h2 id="webapp-api-specification" tabindex="-1">
						Webapp API specification
						<a class="header-anchor" href="#webapp-api-specification" aria-label="Permalink to &quot;Webapp API specification&quot;">​</a>
					</h2>
				</main>
				<WebappApi />
				<VPDocFooter>
					<template #doc-footer-before><slot name="doc-footer-before" /></template>
				</VPDocFooter>
				<slot name="doc-after" />
			</div>
		</div>
	</div>
</div>

<style scoped>
.VPDoc {
	padding: 32px 24px 96px;
	width: 100%;
}

.VPDoc .VPDocOutlineDropdown {
	display: none;
}

@media (min-width: 960px) and (max-width: 1280px) {
	.VPDoc .VPDocOutlineDropdown {
		display: block;
	}
}

@media (min-width: 768px) {
	.VPDoc {
		padding: 48px 32px 128px;
	}
}

@media (min-width: 960px) {
	.VPDoc {
		padding: 32px 32px 0;
	}

	.VPDoc:not(.has-sidebar) .container {
		display: flex;
		justify-content: center;
		max-width: 992px;
	}

	.VPDoc:not(.has-sidebar) .content {
		max-width: 752px;
	}
}

@media (min-width: 1280px) {
	.VPDoc .container {
		display: flex;
		justify-content: center;
	}
}

@media (min-width: 1440px) {
	.VPDoc:not(.has-sidebar) .content {
		max-width: 784px;
	}

	.VPDoc:not(.has-sidebar) .container {
		max-width: 1104px;
	}
}

.container {
	margin: 0 auto;
	width: 100%;
}

.content {
	position: relative;
	margin: 0 auto;
	width: 100%;
}

@media (min-width: 960px) {
	.content {
		padding: 0 32px 128px;
	}
}

@media (min-width: 1280px) {
	.content {
		order: 1;
		margin: 0;
		min-width: 640px;
	}
}

.content-container {
	margin: 0 auto;
}
</style>
