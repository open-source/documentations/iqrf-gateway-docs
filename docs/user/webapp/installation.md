# How to install the webapp

Install first [IQRF Gateway Daemon](/user/daemon/installation) package since
IQRF repository is added during the daemon installation.

IQRF repository is also needed in order to install webapp interface.

## IQRF Gateway repository

The web guide is available at <https://repos.iqrf.org>. The repository should be already
added at this point since it is needed for the daemon installation.

## Install IQRF Gateway webapp

```bash
sudo apt-get install iqrf-gateway-webapp
```

## Direct links

Packages and tarballs for download.

- <https://dl.iqrf.org/iqrf-gateway-webapp>
