# Roadmap

## Releases

### IQRF Gateway Daemon

v3.0.x (Q1/2025)

* Reworked lite DB to store network and devices information
* IQMESH network service for IQRF Sensor standard devices
* Improving and hardening JSON schemas
* Updating and extending API tests
* Multiple internal buffers to handle JSON API
* GPIO interface via libgpiod library

### IQRF Gateway Webapp

v3.0.x (Q1/2025)

* Interface to reworked lite DB
* IQRF Sensor network service setting
* Vuetify frontend
* Adding integration and API tests
* Adding widgets and hints for better orientation
