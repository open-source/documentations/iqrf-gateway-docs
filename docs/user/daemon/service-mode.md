# Service mode

Use the power of [IQRF IDE](https://iqrf.org/technology/iqrf-ide) and its IQMESH
Network Manager to control your network. To connect to gateway from IQRF IDE, use Daemon's UDP channel.

## Mode change

IQRF Gateway Webapp (/gateway/change-mode/) can be used to switch the gateway
into **service** mode.
Mode switching can be also done
via [management API](https://apidocs.iqrf.org/iqrf-gateway-daemon/latest/json/iqrf/examples/mngDaemon_Mode-request-1-0-0-example.json)
.

## Using IQRF IDE

![IDE connection to the gateway](/user/ide-udp-gw.png)

* Select Mode: **User gateway**.

## Using JSON API

Once you have finished working with IQMESH Network Manager switch the gateway
back to **operational**
mode. Channels MQ/WS/MQTT are active in **operational** mode.
