# Daemon API

JSON messages for communication
via [MQ](https://en.wikipedia.org/wiki/Message_queue),
[WebSocket](https://en.wikipedia.org/wiki/WebSocket) or
[MQTT](https://cs.wikipedia.org/wiki/MQTT) channels.
Messages have been tested with OS v4.02D and DPA v3.02 at TR-7xD and higher.

Check with
[DPA release](https://www.iqrf.org/DpaTechGuide/pages/document-revisions.html)
pages the relevance of particular DPA commands. Some DPA commands were
updated/added/removed in further versions of DPA.

IQRF Gateway Daemon API categories:

![IQRF GWD API categories](/user/iqrfgd-api.png)

Release API:

- [v2.6.x](/user/daemon/api/v260)
- [v2.5.x](/user/daemon/api/v250)
- [v2.4.x](/user/daemon/api/v240)
- [v2.3.x](/user/daemon/api/v230)
