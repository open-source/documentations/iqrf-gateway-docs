# Installing Daemon

## Experimental Features

Major new features in the IQRF Gateway Daemon are introduced as experimental
features at first. This allows you to play around with them and test them out
in a developmental capacity without having it affect production targets.

Once the feature is fully tested we will release it as a production feature.

### Alpha

The feature is still under development and should not be used on production
targets as the underlying functionality as well as the APIs may change in future
releases.

## Add IQRF Gateway repository

**Follow** the web guide at <https://repos.iqrf.org>.

### Stable

- iqrf-gateway-daemon_2.5.*_amd64.deb
- iqrf-gateway-daemon_2.5.*_i386.deb
- iqrf-gateway-daemon_2.5.*_arm64.deb
- iqrf-gateway-daemon_2.5.*_armhf.deb
- iqrf-gateway-daemon_2.5.*_armel.deb

### Devel (alpha)

- iqrf-gateway-daemon_3.0.0-*_amd64.deb
- iqrf-gateway-daemon_3.0.0-*_i386.deb
- iqrf-gateway-daemon_3.0.0-*_arm64.deb
- iqrf-gateway-daemon_3.0.0-*_armhf.deb
- iqrf-gateway-daemon_3.0.0-*_armel.deb

## Stop and disable the daemon v1

If there is IQRF Gateway Daemon v1 already running in the system.

```bash
sudo systemctl stop iqrf-daemon
sudo systemctl disable iqrf-daemon
```

or

```bash
sudo systemctl disable --now iqrf-daemon
```

## Install the daemon

```bash
sudo apt-get install iqrf-gateway-daemon
```

or **update** if the daemon is already installed.

```bash
sudo apt-get update
sudo apt-get --only-upgrade install iqrf-gateway-daemon
```

### Update from beta release

```bash
sudo apt-get purge iqrf-gateway-daemon
sudo apt-get install iqrf-gateway-daemon=2.5.*
```

## Check the status of the daemon

```bash
sudo systemctl status iqrf-gateway-daemon.service
```

## Direct links

Packages and tarballs for download.

- <https://dl.iqrf.org/iqrf-gateway-daemon>
