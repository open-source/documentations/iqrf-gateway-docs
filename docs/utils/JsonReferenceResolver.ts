import { resolveInlineRef } from '@stoplight/json';
import { type Dictionary } from '@stoplight/types';

/**
 * JSON Reference information
 */
export declare interface ReferenceInfo {
	/// JSON Pointer
	pointer: string | null;
	/// JSON Reference source
	source: string | null;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export declare type ReferenceResolver = (ref: ReferenceInfo, propertyPath: string[] | null, currentObject?: object) => any;

/**
 * JSON Reference resolver
 */
export class JsonReferenceResolver {

	/**
	 * Resolves JSON references
	 * @param {object|undefined} contextObject The object to resolve references against
	 * @return {ReferenceResolver} The reference resolver
	 */
	public static resolve(contextObject: object|undefined): ReferenceResolver {
		return function ({ pointer }, _, currentObject) {
			const activeObject: object = contextObject ?? currentObject ?? {};

			if (pointer === null) {
				return null;
			}

			if (pointer === '#') {
				return activeObject;
			}

			const resolved = resolveInlineRef(activeObject as Dictionary<string>, pointer);

			if (resolved) {
				return resolved;
			}

			throw new ReferenceError(`Could not resolve '${pointer}`);
		};
	}

}
