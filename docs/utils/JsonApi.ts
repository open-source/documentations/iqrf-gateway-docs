import { compare } from 'compare-versions';
import { inject, ref, type Ref } from 'vue';

import {
	type IJsonApiBadges,
	type IJsonApiChange,
	type IJsonApiGroup,
	type IJsonApiItem,
	type IJsonApiVersion,
	type IJsonApiVersions,
	type IVersionParts,
	JsonApiItemTypes,
	JsonApiLinkTypes,
} from '../types/JsonApi';

export default class JsonApiUtils {

	/**
	 * Compares IQRF Gateway Daemon version
	 * @param {IJsonApiVersion} version Supported version
	 * @param {string|null} daemonVersion IQRF Gateway Daemon version
	 * @return {boolean} Is version supported?
	 */
	public static compareDaemonVersion(version: IJsonApiVersion, daemonVersion: string|null = null): boolean {
		if (daemonVersion !== null) {
			return compare(daemonVersion, version.version, version.operator);
		}
		const refDaemonVersion: Ref<string> = inject('daemonVersion') ?? ref('99.99.99');
		return compare(refDaemonVersion.value, version.version, version.operator);

	}

	/**
	 * Checks if group or item is enabled
	 * @param {IJsonApiVersion|undefined} versions Supported versions
	 * @param {string|null} daemonVersion IQRF Gateway Daemon version
	 * @return {boolean} Is group or item enabled?
	 */
	public static isEnabled(versions: IJsonApiVersions|undefined, daemonVersion: string|null): boolean {
		if (versions === undefined) {
			return true;
		}
		const daemonVersionParts = JsonApiUtils.getVersionParts(daemonVersion);
		for (const version of versions.daemon) {
			const versionParts = JsonApiUtils.getVersionParts(version.version);
			if (!JsonApiUtils.compareDaemonVersion(version, daemonVersion)) {
				return versionParts?.major === daemonVersionParts?.major &&
					versionParts?.minor === daemonVersionParts?.minor &&
					version.operator !== '<';
			}
		}
		return true;
	}

	/**
	 * Returns badges
	 * @param {IJsonApiItem | IJsonApiGroup} apiItem Daemon API item
	 * @param {string|null} daemonVersion IQRF Gateway Daemon version
	 * @return {IJsonApiBadges} Badges
	 */
	public static getBadges(apiItem: IJsonApiItem | IJsonApiGroup, daemonVersion: string|null): IJsonApiBadges {
		const badges: IJsonApiBadges = {};
		if (daemonVersion === '3.0.0') {
			Object.assign(badges, apiItem.badges);
		}
		if ('type' in apiItem) {
			badges.async = apiItem.type === JsonApiItemTypes.AsyncResponse;
		}
		if ('docs' in apiItem) {
			badges.docs = apiItem.docs;
		}
		const versions = apiItem.versions;
		if (versions === undefined) {
			return badges;
		}
		const daemonVersionParts = JsonApiUtils.getVersionParts(daemonVersion ?? '1.0.0');
		for (const version of versions.daemon) {
			const versionParts = JsonApiUtils.getVersionParts(version.version);
			if (
				versionParts?.minor === daemonVersionParts?.minor &&
				JsonApiUtils.compareDaemonVersion({ version: daemonVersion ?? '1.0.0', operator: '>=' }, version.version)
			) {
				badges.daemon = { sinceVersion: version.version };
				break;
			}
		}
		for (const version of versions.dpa ?? []) {
			badges.dpa = {};
			if (version.operator === '<') {
				badges.dpa.untilVersion = version.version;
			}
			if (version.operator === '>=') {
				badges.dpa.sinceVersion = version.version;
			}
		}
		return badges;
	}

	/**
	 * Returns relevant changes
	 * @param {IJsonApiItem | IJsonApiGroup} apiItem Daemon API item
	 * @param {string|null} daemonVersion IQRF Gateway Daemon version
	 * @return {IJsonApiChange[]} Changes
	 */
	public static getChanges(apiItem: IJsonApiItem|IJsonApiGroup, daemonVersion: string|null): IJsonApiChange[] {
		if (apiItem.changelog) {
			for (const entry of apiItem.changelog) {
				if (entry.version === daemonVersion) {
					return entry.changes;
				}
			}
		}
		return [];
	}

	/**
	 * Returns IQRF Gateway Daemon version parts
	 * @param {string|null} version IQRF Gateway Daemon version
	 * @return {IVersionParts|null} IQRF Gateway Daemon version parts
	 */
	public static getVersionParts(version: string|null): IVersionParts|null {
		if (version === null) {
			return null;
		}
		const parsed = /(?<major>\d).(?<minor>\d).(?<patch>\d)/.exec(version);
		if (parsed?.groups === undefined) {
			return null;
		}
		return {
			major: Number.parseInt(parsed.groups.major),
			minor: Number.parseInt(parsed.groups.minor),
			patch: Number.parseInt(parsed.groups.patch),
		};
	}

	/**
	 * Returns link to JSON API JSON file
	 * @param {JsonApiLinkTypes} type JSON API link type {@link JsonApiLinkTypes}
	 * @param {string} daemonVersion IQRF Gateway Daemon version
	 * @param {string} mType Message type
	 * @param {string} version Message version
	 * @param {boolean} raw Return raw JSON schema? Available only for schema types.
	 * @return {string} Link to JSON API JSON file
	 */
	public static getLink(type: JsonApiLinkTypes, daemonVersion: string, mType: string, version: string, raw = false): string {
		let baseUrl = `https://apidocs.iqrf.org/iqrf-gateway-daemon/${daemonVersion}/json`;
		if (daemonVersion === 'v300') {
			baseUrl = 'https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/-/raw/v3.x-dev/api';
		}
		version = version.replaceAll('.', '-');
		let fileName: string;
		if ([JsonApiLinkTypes.RequestExample, JsonApiLinkTypes.RequestSchema].includes(type)) {
			fileName = `${mType}-request-${version}`;
		} else if ([JsonApiLinkTypes.ResponseExample, JsonApiLinkTypes.ResponseSchema].includes(type)) {
			fileName = `${mType}-response-${version}`;
		} else if ([JsonApiLinkTypes.MessageExample, JsonApiLinkTypes.MessageSchema].includes(type)) {
			fileName = `${mType}-message-${version}`;
		} else {
			throw new Error('Unknown link type');
		}
		if ([JsonApiLinkTypes.MessageExample, JsonApiLinkTypes.RequestExample, JsonApiLinkTypes.ResponseExample].includes(type)) {
			if (daemonVersion === 'v300') {
				return `${baseUrl}/examples/${fileName}-example.json`;
			}
			return `${baseUrl}/iqrf/examples/${fileName}-example.json`;
		}
		if (daemonVersion === 'v300') {
			return `${baseUrl}/${fileName}.json`;
		}
		if (raw) {
			return `${baseUrl}/iqrf/${fileName}.json`;
		}
		return `${baseUrl}#iqrf/${fileName}.json`;
	}

	/**
	 * Returns link to JSON schema reference file
	 * @param {string} daemonVersion IQRF Gateway Daemon version
	 * @return {string|null} Link to JSON schema reference file
	 */
	public static getJsonReferenceLink(daemonVersion: string): string | null {
		if (daemonVersion === 'v300') {
			return 'https://gitlab.iqrf.org/open-source/iqrf-gateway-daemon/-/raw/v3.x-dev/api/definitions.json';
		}
		return null;
	}

}
